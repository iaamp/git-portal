#!/usr/bin/env bash
# bashy portal gun for git
# by klaatu at member.fsf.org

# GPLv3
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://gnu.org/licenses

# requires:
# - bash
# - git
# - dirname
# - pushd,popd

CWD=$(pwd)
TOP=`git rev-parse --show-toplevel || false`
RM=${RM:-`which rm`}
PORTAL=${PORTAL:-.portal}
RED='\033[0;31m'
NOR='\033[0m' 

set -e

help() {
    echo "git-portal: a portal-gun for git"
    echo " "
    echo "git-portal [init|add|rm|status] FILE"
    echo "git-portal init    start using git-portal in a git repo"
    echo "git-portal add     send a file through the portal"
    echo "git-portal rm      remove a file from git-portal *and* from git"
    echo "git-portal status  review files in the portal and find broken links"
    exit
}

init() {
    # create a $PORTAL dir
    # and backup location
    # and add those to .gitignore
    mkdir "${TOP}"/"${PORTAL}" 2> /dev/null || true
    touch "${TOP}"/.gitignore
    install -m 750 -D __datadir__/git-portal/hooks/pre-push "${TOP}"/.git/hooks/pre-push
    install -m 750 -D __datadir__/git-portal/hooks/post-merge "${TOP}"/.git/hooks/post-merge
    IGN=`grep "${PORTAL}" "${TOP}"/.gitignore` || echo ""${PORTAL}"" >> "${TOP}"/.gitignore
    if [ -n "$VERBOSE" ]; then echo "git-portal is now active in this project"; fi
    exit 0
}

add() {
    # cp file and attrs to $PORTAL location
    # ln working file to this location
    local DEST=`dirname "${item}"`
    local REF=`readlink -f ${item}`
    local GITREF=`git rev-parse --show-prefix`

    if [ -d "${item}" ]; then
        # do not portalize a dir
        if [ -n "$VERBOSE" ]; then echo "Directories cannot go through the portal. Sending files instead."; fi
        # build dir infrastructure in portal
        find "${item}" -type d -exec \
            install -d {} "${TOP}"/"${PORTAL}"/"${GITREF}"/{} \;
        # sprinkle all files into appropriate _portal dirs
        find "${item}" -type f -exec \
            install -D {} "${TOP}"/"${PORTAL}"/"${GITREF}"/{} \;
        # # create symlinks where the originals came from
        find "${item}" -type f -exec \
            ln -srf "${TOP}"/"${PORTAL}"/"${GITREF}"/{} {} \;
    else
        # this is a file not a dir
        install -D "${item}" "${TOP}"/"${PORTAL}"/"${GITREF}${item}"
        ln -rsf "${TOP}"/"${PORTAL}"/"${GITREF}${item}" "${item}"
        if [ -n "$VERBOSE" ]; then echo "${item} has gone through the portal. You may git add its doppelganger now."; fi
    fi
}

gitrm() {
    local DEST=`dirname "${item}"`
    if [ -n "$VERBOSE" ]; then echo "${item}"; fi
    if [ -n "$VERBOSE" ]; then echo "git rm -rf ${REF}"; fi
    # move the portal item back to its original location, destroying symlink
    mv "${TOP}"/"${PORTAL}"/"${item}" "${TOP}"/"${item}"
    # git-rm the file
    git rm -rf "${TOP}"/"${item}"
}

gitstat() {
    find "${TOP}" -type l -exec file {}  \; | \
    awk '
/broken/ { hit["$0"]=$0; }; 
!/broken/ { print; }; 
END { for (k in hit) { 
printf ("'${RED}'%s\n",hit[k]) ;}
;}'
#restore normal colour
printf ${NOR}
}

## parse
while [ True ]; do
if [ "$1" = "init" ]; then
    init
    shift 1
elif  [ "$1" = "-v" -o "$1" = "--verbose" ]; then 
    VERBOSE=1
    VV="-v"
    shift 1
elif  [ "$1" = "add" -o "$1" = "--add" ]; then 
    ADD=1
    shift 1
elif  [ "$1" = "rm" ]; then 
    GITRM=1
    shift 1
elif  [ "$1" = "status" -o "$1" = "--status" ]; then 
    GITSTAT=1
    shift 1
elif  [ "$1" = "-f" -o "$1" = "--force" ]; then 
    FORCE=1
    shift 1
elif  [ "$1" = "clean" -o "$1" = "--clean" ]; then 
    CLEAN=1
    shift 1
elif  [ "$1" = "help" -o "$1" = "--help" -o "$1" = "-h" -o "$1" = "--options" ]; then 
    help
    shift 1
else
    break
fi
done

ARG=( "${@}" ) #create array, retain spaces
set -e
ARRAYSIZE=${#ARG[*]}

#n=0

## main loop
while [ True ]; do
    for item in "${ARG[@]}"; do

    #unset ARG[${n}]
    #let n++

    if [ "X$ADD" == "X1" ]; then
        add "${item}"
    fi

    if [ "X$GITRM" == "X1" ]; then
        gitrm "${item}"
    fi

    done
    break
done

# TODO this is wrong if adding many files at once, e.g. by using git-portal add *.jpg
if [ "$ARRAYSIZE" -gt 3 ]; then
    echo "too many arguments"
    exit
fi

if [ "X$GITSTAT" == "X1" ]; then
    gitstat
fi

if [ "X$CLEAN" == "X1" ]; then
    clean
fi

exit
